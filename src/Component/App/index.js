/**
 * Created by user on 2018/2/27.
 */
import style from "./index.scss";
const angular = require('angular');
import $routeConfig from "@/Route";

const name='app';
class controller{
  constructor() {
    this.style = style['panel-app'];
    this.name = 'angular&es6';
    this.firstName = 'zhang';
    this.lastName = 'tony';
  }

  getName() {
    return this.name;
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  $onInit() {

  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }
}
import pageIndexHome from '../pageIndexHome';
export default angular
  .module(name, [
    pageIndexHome
  ])
  .component(name,{
    template:'<ng-outlet></ng-outlet>',
    controllerAs: "m",
    controller,
    $routeConfig,
    bindings: {
      year: '<',
      month: '<'
    }
  })
  .name;

