/**
 * Created by user on 2018/2/27.
 */
import _ from 'lodash';
import style from "./index.scss";
const angular = require("angular");
import fs from 'fs';
const template = fs.readFileSync(__dirname+'/index.html', 'utf8');
import $routeConfig from "@/Route";

const name='pageIndexHome';
//设置当前组件路由
$routeConfig.push({ path: `/${name}`, component:name, name: _.upperFirst(name) });


class controller{
  constructor() {
    this.name = name;
    this.style = style['panel-app'];
    this.firstName = 'zhang';
    this.lastName = 'tony';
    this.lists = [
      {name:'a'},
      {name:'b'},
      {name:'c'},
      {name:'d'}
    ];
  }


  getName() {
    return this.name;
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  $onInit() {

  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }

}

export default angular
  .module(name, [])
  .component(name,{
    template,
    controllerAs: "m",
    controller,
    bindings: {
      year: '<',
      month: '<'
    }
  })
  .name;

