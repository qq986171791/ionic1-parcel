export default {
    subscribe(store) {
        this.set(store);
        store.subscribe(() => {
            this.set(store);
        })
    },
    // 将状态记录到 localStorage
    set(store){
        let data = store.getState(),
            appName = data.common.appName;
        data = JSON.stringify(data)
        data = encodeURIComponent(data)
        if (window.btoa) {
            data = btoa(data)
        }
        window.localStorage.setItem(appName, data)
    },
    get(store){
        let common = store.getState().common;
        let data = window.localStorage.getItem(common.appName);
        if(window.atob){
            data = atob(data);
        }
        data = decodeURIComponent(data);
        data = JSON.parse(data);
        return data;
    }
}
