/**
 * Created by user on 2018/4/2.
 */
const angular = require("angular");
import PageUserLogin from './Component/PageUserLogin'
import PageUserEnroll from './Component/PageUserEnroll'

const name = 'user';

export default angular
    .module(name, [PageUserLogin,PageUserEnroll]).name;