const angular = require('angular')
import erhuUi from '@/erhu-ui';
import App from '@/Component/App';
import User from '@/Module/User'
import rootReducer from '@/Redux/reducer';
import ngRedux from 'ng-redux';
import IonitInit from '@/ionicInit'
import thunk from 'redux-thunk';



const m = angular.module('ngApp', [
    ngRedux,
    erhuUi,
    User,
    'ionic',
    require('ngComponentRouter'),
    App
]);
m.config(['$ngReduxProvider',($ngReduxProvider) => {
    $ngReduxProvider.createStoreWith(rootReducer,[thunk]);
}])
m.run(IonitInit)
m.value('$routerRootComponent', App)
