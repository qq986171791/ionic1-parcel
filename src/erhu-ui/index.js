/**
 * Created by user on 2018/4/2.
 */
import EhLogin from './packages/EhLogin';
import EhEnroll from './packages/EhEnroll';
const angular = require("angular");

const name = 'eh';

export default angular
    .module(name, [EhLogin,EhEnroll]).name;
