/**
 * Created by user on 2018/2/27.
 */
import style from "./index.scss";
const angular = require("angular");
import fs from 'fs';
const template = fs.readFileSync(__dirname+'/index.html', 'utf8');
import redux from '@/Redux'
import utils from '@/utils'
import _ from 'lodash';

const name='ehEnroll';

@redux
class controller{
  constructor($ionicPopup) {
    this.name = name;
    this.style = style['panel-app'];
    this.firstName = 'zhang';
    this.lastName = 'tony';
    this.$ionicPopup = $ionicPopup;
  }

  getName() {
    return this.name;
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
  //注册一个用户
  async doEnroll(opts) {
      const user = Object.assign({
          username: '',
          password: ''
      }, opts)
      if (utils.isEmpty(user.username)) {
          var alertPopup = this.$ionicPopup.alert({
              title: '错误',
              template: '用户名不能为空'
          });
          alertPopup.then((res) => {

          });
          return;
      }
      if (!utils.isEmail(user.username)) {
          var alertPopup = this.$ionicPopup.alert({
              title: '错误',
              template: '邮箱格式不正确'
          });
          alertPopup.then((res) => {
              if (res) {
                  this.email = "";
              }
          });
          return;
      }
      if (utils.isEmpty(user.password)) {
          var alertPopup = this.$ionicPopup.alert({
              title: '错误',
              template: '密码不能为空'
          });
          alertPopup.then((res) => {

          });
          return;
      }
      if (!utils.isPassword(user.password)) {
          var alertPopup = this.$ionicPopup.alert({
              title: '错误',
              template: '密码至少包含一个小写字母和大写字母及数字'
          });
          alertPopup.then((res) => {
              if (res) {
                  this.password = "";
              }
          });
          return;
      }
      const res = await this.addUser(user)
      var alertPopup = this.$ionicPopup.alert({
          title: res.popup.title,
          template: res.popup.message
      });
      alertPopup.then((res) => {});
  }


  $onInit() {
    console.log(this);
  }

  $onChanges(changesObj) {

  }

  $onDestroy() {

  }

  $postLink() {

  }

}
controller.$inject = ['$ionicPopup'];
export default angular
  .module(name, [])
  .component(name,{
    template,
    controllerAs: "m",
    controller,
    bindings: {
      year: '<',
      month: '<'
    }
  })
  .name;

