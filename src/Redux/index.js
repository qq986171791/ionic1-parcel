/**
 * Created by user on 2018/3/1.
 */
import injector from '@/injector';
function decorateBeforeFn(bFn,fn) {
  return function (...args) {
    bFn.apply(this,[this]);
    return fn.apply(this,args);
  }
}

function decorateAfterFn(fn,aFn) {
    return function (...args) {
      const ret = fn.apply(this,args);
        aFn.apply(this,[this]);
        return ret;
    }
}

export default function (target, key, descriptor) {
  let $onInit = target.prototype.$onInit;
  let $onDestroy = target.prototype.$onDestroy;
  let $ngRedux = null;
  let disconnect = null;
  target.prototype.$onInit = decorateBeforeFn(function (ins) {
    if(!ins.name){
        throw "eh error 1:component name is not define"
        return;
    }
    $ngRedux = injector.get('$ngRedux');
    disconnect = $ngRedux.connect((state) => {
        return state;
    }, Object.assign(
        require('@/Redux/actions/pageIndexHome'),
        require('@/Redux/actions/pageUserEnroll'),
        require('@/Redux/actions/common'),
    ))(this);
  },$onInit);

  target.prototype.$onDestroy = decorateBeforeFn(() => {
    disconnect();
  },$onDestroy);

  return target;
}
