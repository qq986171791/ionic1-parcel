import _ from 'lodash'
import zh from './i18n/zh';
import en from './i18n/en';
import {SAVE_LANG} from '../actions/common'

let initState = {
    appName:'eh',
    dev:true,
    lang:'zh',
    firebase:{
        apiKey: "AIzaSyBx8qdAKafAIRfiltS-UFNlSEyJMZZxFp4",
        authDomain: "ionic1-parcel.firebaseapp.com",
        databaseURL: "https://ionic1-parcel.firebaseio.com",
        projectId: "ionic1-parcel",
        storageBucket: "ionic1-parcel.appspot.com",
        messagingSenderId: "895277635189"
    },
    i18n:zh,
};

export default (state=initState, action) => {
    switch (action.type) {
        case SAVE_LANG:
            if(action.payload == 'zh'){
                return _.merge(state,{
                    i18n:zh
                })
            }
            if(action.payload == 'en'){
                return _.merge(state,{
                    i18n:en
                })
            }
        default:
            return state;
    }
}
