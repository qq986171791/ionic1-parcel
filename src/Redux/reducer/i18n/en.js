export default {
    http:{
        'ADD_USER':{
            status:{
                success:'sign up success',
                error:'sign up error',
            },
            email:{
                error:'The email address is already in use by another account.',
            }
        },
        'FIND_USER':{
            status:{
                success:'登陆成功',
                err:'登陆失败',
            },
        },
    },
    page:{
        username:'Username',
        password:'Password',
        btnLogin:'login',
        email:'email',
    }
}