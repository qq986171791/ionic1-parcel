export default {
    http:{
        'ADD_USER':{
            status:{
                success:'注册成功',
                err:'注册失败',
            },
            email:{
                err:'email已经被占用',
            }
        },
        'FIND_USER':{
            status:{
                success:'登陆成功',
                err:'登陆失败',
            },
        },
    },
    page:{
        username:'用户名',
        password:'密码',
        btnLogin:'登陆',
        email:'邮箱',
    }
}