import { SELECT_DEVICE } from '../actions/pageIndexHome'
import _ from 'lodash'

let initState = {
    lists:[1,2,3,4,5]
};

export default (state=initState, action) => {
  switch (action.type) {
  case SELECT_DEVICE:
    return _.merge(initState,action.payload);
  default:
    return state;
  }
}
