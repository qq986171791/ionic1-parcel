import { combineReducers } from 'redux';
import pageIndexHome from './pageIndexHome';
import pageUserEnroll from './pageUserEnroll'
import common from './common'

const rootReducer = combineReducers({
    common,
  pageIndexHome,
    pageUserEnroll
});

export default rootReducer;
