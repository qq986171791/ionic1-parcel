export const ADD_USER = 'ADD_USER'
export const FIND_USER = 'FIND_USER'

export function addUserAction(payload) {
  	return {
		type:ADD_USER,
        payload
	}
}

export function findUserAction(payload) {
    return {
        type:FIND_USER,
        payload
    }
}

export function addUser(payload) {
    return (dispatch,getState) => {
        return firebase.auth().createUserWithEmailAndPassword(payload.username, payload.password)
        .catch(
            res => {
                const i18n = getState().common.i18n;
                res.popup = {};
                if(res.code == 'auth/email-already-in-use'){
                    res.status = 1
                    res.popup.title = '错误';
                    res.popup.message = i18n.http[ADD_USER].email.err;
                }
                return res
            }
        )
        .then(
            res => {
                if(1 == res.status){
                    return res;
                }
                const i18n = getState().common.i18n;
                res.popup = {};
                res.popup.title = '消息';
                res.popup.message = i18n.http[ADD_USER].status.success;
                dispatch(
                    addUserAction(
                        {user:res}
                    )
                )
                console.log(getState());
                return res;
            }
        )
    };
}

export function findUser() {
    return (dispatch, getState) => {
        dispatch(findUserAction());
    };
}
