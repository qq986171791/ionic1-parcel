export const SAVE_LANG = 'SAVE_LANG'

export function saveLang(payload) {
    return {
        type:SAVE_LANG,
        payload
    }
}