export const SELECT_DEVICE = 'SELECT_DEVICE'

export function selectDevice(payload) {
  	return {
		type:SELECT_DEVICE,
        payload
	}
}
